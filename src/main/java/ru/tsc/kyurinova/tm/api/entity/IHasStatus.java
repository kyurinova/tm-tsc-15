package ru.tsc.kyurinova.tm.api.entity;

import ru.tsc.kyurinova.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
