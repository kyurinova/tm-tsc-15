package ru.tsc.kyurinova.tm.api.service;

import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    Task bindTaskById(String projectId, String taskId);

    Task unbindTaskById(String projectId, String taskId);

    void removeAllTaskByProjectId(String projectId);

    Project removeById(String projectId);

    List<Task> findAllTaskByProjectId(String projectId);

}
