package ru.tsc.kyurinova.tm.api.repository;

import ru.tsc.kyurinova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
