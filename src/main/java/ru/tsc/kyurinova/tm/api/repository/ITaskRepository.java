package ru.tsc.kyurinova.tm.api.repository;

import ru.tsc.kyurinova.tm.enumerated.Status;

import ru.tsc.kyurinova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    void clear();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    Task startById(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

    Task bindTaskToProjectById(String projectId, String taskId);

    Task unbindTaskToProjectById(String projectId, String taskId);

    Task findByProjectAndTaskId(String projectId, String taskId);

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

    void removeAllTaskByProjectId(String projectId);

    List<Task> findAllTaskByProjectId(String projectId);
}
